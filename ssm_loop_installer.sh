#!/bin/bash
IP_ADDR=(
   10.0.55.87
   10.0.55.92
   10.0.54.173
)
for ip in "${!IP_ADDR[@]}"
 do
    ssh -i ~/key_aws_hosts_id_rsa root@${IP_ADDR[$ip]} 'bash -s' < ~/ssm_install.sh

done



cat <<EOF > ssm_install.sh
#!/bin/bash

if [ `which yum` ]; then
   IS_RHEL=1
mkdir /tmp/ssm
curl https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm -o /tmp/ssm/amazon-ssm-agent.rpm
sudo rpm -i /tmp/ssm/amazon-ssm-agent.rpm
sudo service amazon-ssm-agent stop
sudo service amazon-ssm-agent start
elif [ `which apt` ]; then
   IS_UBUNTU=1
mkdir /tmp/ssm
curl https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb -o /tmp/ssm/amazon-ssm-agent.deb
sudo dpkg -i /tmp/ssm/amazon-ssm-agent.deb
sudo service amazon-ssm-agent stop
sudo service amazon-ssm-agent start
else
   IS_UNKNOWN=1
fi
EOF