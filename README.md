# aws cli snippets

EC2 Snippets:

Describe Security groups with 0.0.0.0 ingress
```bash
aws ec2 describe-security-groups \
--filters Name=ip-permission.from-port,Values=22 \
		  Name=ip-permission.to-port,Values=22 \
		  Name=ip-permission.cidr,Values='0.0.0.0/0' \
--query "SecurityGroups[*].{Name:GroupName}" \
--profile ${AWS_PROFILE}
```

Describe security groups with ingress rules in table format
```bash
aws ec2 \
describe-security-groups \
--group-id $SGID \
--region ap-southeast-2 \
--query "SecurityGroups[].IpPermissions[][].{Protocol:IpProtocol,Range:IpRanges}" \
--output table
```

Describe all Public IPs associated with EC2 instances:
```bash
aws ec2 describe-instances \
--query "Reservations[*].Instances[*].PublicIpAddress" \
--output=text \
--profile ${AWS_PROFILE}
```

Describe all Public IPs associated with an AWS Account:
```bash
aws ec2 describe-network-interfaces \
--query NetworkInterfaces[].Association.PublicIp
```

Get ARN of ACM Certificate bsased on domain name:
```bash
aws acm describe-certificate \
        --certificate-arn $(aws acm list-certificates \
        --query 'CertificateSummaryList[?starts_with(DomainName, `domainname.com`)].CertificateArn' \
        --output text)
```

Cloudwatch:
    Takes a given cloudwatch log group, convertes its output from bytes to GB then prints to stdout:
```bash
aws \
logs \
describe-log-groups \
--log-group-name-prefix "${flowlog}" \
| jq '.logGroups[].storedBytes' \
| awk '{ byte =$1 /1024/1024/1024; print byte " GB" }'
```

S3 Bucket Snippets:
```bash
export bucket-name ="$(aws cloudformation describe-stacks --stack-name “StackWithS3Bucket” \
--output text --query 'Stacks[0].Outputs[?OutputKey==`Test-S3Bucket`].OutputValue')"
```
Display S3 buckets in list format:
```bash
aws s3api list-buckets \
--output json \
| grep "\"Name" \
| cut -d':' -f2 \
| sed -E 's/\"//g'
```

Display S3 bucket and its size:
```bash
aws s3api list-objects \
--bucket some-bucket \
--output json \
--query "[sum(Contents[].Size), length(Contents[])]"
```

Display S3 bucket size, simple way:
```bash
aws s3 ls \
--summarize \
--human-readable \
--recursive s3://some-bucket
```

Display AWS RDS instance Certificates:
```bash
aws rds describe-db-instances \
--db-instance-identifier bioc-sys-ops \
--query "DBInstances[].CACertificateIdentifier"
```

bash exports(creates daily hist file, with date):
```bash
export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; \
then echo "$(date "+%Y-%m-%d.%H:%M:%S") $(tty | sed "s/\/.*\///g") $(pwd) \
$(history 1)" >> ~/lib/logs/bash-history-$(date "+%Y-%m-%d").log; fi'
```

Set linux firectory and file permissions
```bash
sudo chmod 755 $(find "$SOME_DIR" -type d)
sudo chmod 644 $(find "$SOME_DIR" -type f)
```

Updating an AWS Security Group with SSH inbound from public ip:
```bash
#!/usr/bin/env zsh

groupName="inbound-ssh"
pubip=$(curl v4.ifconfig.co) || fail "$@"

log() {
  echo "$1" >&2
}

fail() {
  log "ERROR:"
  exit 1
}


aws \
  ec2 authorize-security-group-ingress \
  --group-name ${groupName} \
  --protocol tcp \
  --port 22 \
  --cidr "${pubip}/32"
```


Azure Query Running VMS showing name and OS:
```bash
az vm list --query "[?provisioningState=='Succeeded'].{ name: name, os: storageProfile.osDisk.osType }"
```

Windows, Join to Domain:
```powershell
[CmdletBinding()]
param(
    [string]
    $DomainName,

    [string]
    $UserName,

    [string]
    $Password
)

try {
    $ErrorActionPreference = "Stop"

    $pass = ConvertTo-SecureString $Password -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential -ArgumentList $UserName, $pass

    Add-Computer -DomainName $DomainName -Credential $cred -ErrorAction Stop

    # Execute restart after script exit and allow time for external services
    $shutdown = Start-Process -FilePath "shutdown.exe" -ArgumentList @("/r", "/t 10") -Wait -NoNewWindow -PassThru
    if ($shutdown.ExitCode -ne 0) {
        throw "[ERROR] shutdown.exe exit code was not 0. It was actually $($shutdown.ExitCode)."
    }
}
catch {
    $_.Exception
}
```

PowerShell Access to OAuth Token in Azure DevOps:
```powershell
$url = "$($env:SYSTEM_TEAMFOUNDATIONCOLLECTIONURI)$env:SYSTEM_TEAMPROJECTID/_apis/build/definitions/$($env:SYSTEM_DEFINITIONID)?api-version=4.1-preview" `
Write-Host "URL: $url" $pipeline = Invoke-RestMethod -Uri $url -Headers @{ Authorization = "Bearer $env:TOKEN" } `
Write-Host "Pipeline = $($pipeline | ConvertTo-Json -Depth 100)"
```
list AWS IAM Account Aliases using go:

```go
package main

import (
    "fmt"

    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/iam"
)


func main() {
    sess, err := session.NewSession(&aws.Config{
        Region: aws.String("ap-southeast-2")},
    )

    // Creates an IAM service client.
    svc := iam.New(sess)
    result, err := svc.ListAccountAliases(&iam.ListAccountAliasesInput{
        MaxItems: aws.Int64(10),
    })

    if err != nil {
        fmt.Println("Error", err)
        return
    }

    for i, alias := range result.AccountAliases {
        if alias == nil {
            continue
        }
        fmt.Printf("Alias %d: %s\n", i, *alias)
    }
}
```

Start a powershell process, listening on port 2222, with NoExit Set

```
Start-Process powershell.exe -ArgumentList '-NoExit -command $listener = [System.Net.Sockets.TcpListener]2222; $Listener.Start()'
```

Nmap: Ideintify whois info for IP address:

```bash
nmap --script=asn-query,whois,ip-geolocation-maxmind 1.1.1.1/32
```
Locate DNS reflection servers:

```bash
nmap -sU -A -PN –n –pU:19,53,123,161 –script=ntp-monlist,dns-recursion,snmp-sysdescr SOME.RANGE.HERE/PREFIX
```

CloudWatch Insights: Query to identify src ip of specified address

```sql
fields @timestamp, @message, srcAddr
| sort @timestamp desc
| filter srcAddr like /(^122\.100\.[.]*)/
```
